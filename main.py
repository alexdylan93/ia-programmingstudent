import flask
from flask import request, jsonify, abort
from flask_cors import CORS, cross_origin
from evolutionary_algorithm import AG
from evolutionary_algorithm import Map
import json

app = flask.Flask(__name__)
app.config["DEBUG"] = False
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/IA', methods=['GET'])
def home():
    difficulty = ""
    if 'difficulty' in request.args:
        difficulty = request.args['difficulty']
    else:
        return "Error: No difficulty field provided. Please specify an difficulty."
    return algorithm(difficulty)
    
def algorithm(difficulty):
    print(difficulty)
    alelos = 0 #Tamaño de matriz
    diff = 0 #Definir los obstaculos
    energia = 0
    instrucciones = 0
    limit_break = 3.75 #Umbral de aceptación 
    if difficulty == "easy":    
        alelos = 7
        diff = [1, 3] 
        energia = 3 
        instrucciones = 8
    elif difficulty == "mid":   
        alelos =  8
        diff = [2, 5]
        energia = 9
        instrucciones = 13
    else:                       
        alelos = 9
        diff = [3, 6]
        energia = 15
        instrucciones = 17
        limit_break = 3.7
    problema = Map(alelos, diff, energia, instrucciones)
    individuos = 15
    generaciones = 1000
    factor_mutacion = float(0.125)
    ag = AG(individuos, alelos, generaciones, factor_mutacion, problema, limit_break)
    matrix, camino = ag.run()
    inicio, final, energia, instrucciones = problema.returnData()
    matrix = matrix.tolist()
    instrucciones += calcularInstrucciones(matrix, camino)
    ruta = list(camino)
    response = { 
        "matrix": matrix,
        "inicio": inicio,
        "final": final, 
        "energia": energia,
        "instrucciones": instrucciones,
        "ruta": ruta
    } 
    return json.dumps(response)

def calcularInstrucciones(matriz, camino):
    instrucciones = 0
    anterior = camino[0]
    avanceAnterior = "x"
    for e in camino[1:]:
        if e[0] == anterior[0]: 
            if avanceAnterior == "x": 
                instrucciones += 1
            avanceAnterior = "y"
        else: 
            if avanceAnterior == "y": 
                instrucciones+=1
            avanceAnterior = "x"
        anterior = e
        if matriz[e[0]][e[1]] != 0: 
            instrucciones+=1  
    return instrucciones

app.run()