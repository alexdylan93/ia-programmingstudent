import copy
import numpy as np
import math
import heapq
import threading
import time


class Individuo:
    def __init__(self, alelos, cromosoma):
        self._alelos = alelos
        self._cromosoma = cromosoma
        self._costo = []
        self._fitness = 0

class AG:
    def __init__(self, cantidad_individuos, alelos, generaciones, p, problema, limit_break):
        self._cantidad_individuos = cantidad_individuos
        self._alelos = alelos
        self._generaciones = generaciones
        self._p = p
        self._problema = problema
        self._individuos = np.array([])
        self._limit_break = limit_break

    def run(self):
        self.crearIndividuos()
        self._mejor_historico = copy.deepcopy(self._individuos[0])
        generacion = 1
        mismo_padre = 0
        while generacion <= self._generaciones:
            self.evaluacionTh()
            hijos = np.array([])
            while len(hijos) < len(self._individuos):
                padre1 = self.ruleta()
                padre2 = self.ruleta()
                while padre1 == padre2 and mismo_padre < 100:
                    padre2 = self.ruleta()
                    mismo_padre += 1
                mismo_padre = 0
                h1, h2 = self.cruza(self._individuos[padre1], self._individuos[padre2])
                hijos = np.append(hijos, [h1])
                hijos = np.append(hijos, [h2])
            self.mutacion(hijos)
            self._individuos = np.copy(hijos)
            self._individuos[np.random.randint(len(self._individuos))] = copy.deepcopy(self._mejor_historico)

            if self._mejor_historico._fitness >= self._limit_break:
                print("Generación: ", generacion, 'Mejor Histórico: \n', self._mejor_historico._cromosoma, 'Fitness: ', self._mejor_historico._fitness)
                print("Costo: ", self._mejor_historico._costo)
                break
            if generacion % 100 == 0:
                print("Generación: ", generacion, 'Mejor Histórico: \n', self._mejor_historico._cromosoma, 'Fitness: ', self._mejor_historico._fitness)
                print("Costo: ", self._mejor_historico._costo)
            generacion += 1
        print( "Inicio: ", [int(self._problema._inicio), 0], "| Final: " , [int(self._problema._final), self._problema._tamanio-1])
        print("Tamaño del camino: ", len(self._mejor_historico._costo[1]))
        print("Instrucciones con distancia euclidiana: ", self._problema._instrucciones)
        return self._mejor_historico._cromosoma, self._mejor_historico._costo[1]

# inicializar una matriz en ceros
# la dificultad definirá el tamaño de las matrices: 6, 9, 12
# por cada row y dependiendo la dificultad: (easy 2), (mid 3) (hard 5)
# column  0 inicio, al azar, column final poner el final al azar
    def crearIndividuos(self):
        for _ in range(self._cantidad_individuos):
            cromosoma = np.zeros((self._alelos , self._alelos))
            for _ in range(0, self._problema._obstaculos): #insertar enemigos
                column = np.random.randint(self._alelos, size = 1)
                row =  np.random.randint(self._alelos, size = 1)
                value = np.random.randint( 0, self._problema._dificultad[1], size = 1)
                cromosoma[row, column] = value
            cromosoma[self._problema._inicio, 0] = 0
            cromosoma[self._problema._final, self._problema._tamanio - 1] = 0
            individuo = Individuo(self._alelos, cromosoma)
            self._individuos = np.append(self._individuos, [individuo])

    def evaluacionTh(self):
        threads = []
        for index, individuo in enumerate(self._individuos):
            th = evaluacionThread(index, self._problema._tamanio, self._problema._energia, self._problema._instrucciones,
                                    individuo._cromosoma, self._problema._inicio, self._problema._final, self._problema._obstaculos)
            threads.append(th)

        for th in threads:
            th.start()

        for th in threads:
            th.join()

        for th, ind in zip(threads, self._individuos):
            ind._fitness = th._fitness
            ind._costo = th._costo
            if ind._fitness > self._mejor_historico._fitness:
                self._mejor_historico = copy.deepcopy(ind)

    def ruleta(self):
        f_sum = np.sum([i._fitness for i in self._individuos])
        if f_sum == 0:
            return np.random.randint(len(self._individuos))
        else:
            r = np.random.randint(f_sum + 1)
            k = 0
            F = self._individuos[k]._fitness
            while F < r and k < len(self._individuos) - 1:
                k += 1
                F += self._individuos[k]._fitness
            return k

    def cruza(self, i1, i2):
        h1 = copy.deepcopy(i1)
        h2 = copy.deepcopy(i2)
        s = self._alelos - 1
        punto_cruza = np.random.randint(s) + 1
        h1._cromosoma[punto_cruza:][0:], h2._cromosoma[punto_cruza:][0:] = h2._cromosoma[punto_cruza:][0:], h1._cromosoma[punto_cruza:][0:]
        return h1, h2

    def mutacion(self, hijos):
        for h in hijos:
            for i in range(0, self._alelos):
                for j in range(0, self._alelos):
                    if np.random.rand() < self._p:
                        h._cromosoma[i][j] = np.random.randint(0, self._problema._dificultad[1], size = 1)

class Map:
    def __init__(self, tamanio, dificultad, energia, instrucciones):
        self._mejorCosto = []
        self._tamanio = tamanio
        self._dificultad = dificultad
        self._energia = energia
        self._inicio = np.random.randint(tamanio, size = 1)
        self._final = np.random.randint(tamanio, size = 1)
        self._instrucciones = abs(tamanio) + abs(self._final - self._inicio) + instrucciones - 1
        self._obstaculos = math.trunc(((tamanio * tamanio) * 70) / 100)

    def returnData(self):
        return int(self._inicio), int(self._final), self._energia, int(self._instrucciones)

class evaluacionThread (threading.Thread):
    def __init__(self, threadID, tamanio, energia, instrucciones, cromosoma, inicio, final, obstaculos):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self._tamanio = tamanio
        self._energia = float(energia)
        self._instrucciones = instrucciones
        self._cromosoma = cromosoma
        self._obstaculos = obstaculos
        self._inicio = inicio
        self._final = final
        self._fitness = float(0)
        self._costo = []

    def run(self):
        fitness = float(0)
        d = float(0)
        costo = []
        for i in range(0, self._tamanio):
            for j in range(0, self._tamanio):
                if self._cromosoma[i, j] != 0:
                    d = d + 1
        if d > self._obstaculos:
            fitness = float(fitness + (1/d))
        else:
            if self._cromosoma[self._inicio, 0] == 0:
                fitness = float(fitness + 0.5)
            if self._cromosoma[self._final, self._tamanio - 1] == 0:
                fitness = float(fitness + 0.5)
            costo = self.costeUniforme()
            if costo == True:
                self._fitness = float(fitness)
                self._costo = []
                return
            if float(costo[0]) > self._energia:
                fitness = float(fitness - (1 / float(costo[0]) ))
            else:
                fitness = float(fitness + (float(costo[0])/self._energia))
            if ( len(costo[1]) - 1) > self._instrucciones:
                fitness = float(fitness - (1 / float( len(costo[1]) - 1 )))
            else:
                fitness = float(fitness + ( float(len(costo[1]) - 1) / self._instrucciones))
            fitness = float(fitness + (d/self._obstaculos) )
    #         #¡ATENCIÓN! NO BORRAR ESTAS LINEAS DE CÓDIGO
    #         #fitness = fitness - ( self._instrucciones - (len(costo[1])-1) )
    #         #fitness = fitness + (self._energia - costo[0])
    #         fitness = fitness + d
        self._fitness = float(fitness)
        self._costo = costo

    def costeUniforme(self):
        inicio = [int(self._inicio), 0]
        final = [int(self._final), int(self._tamanio) - 1]
        costo = 0
        actual = []
        visitados  = []
        frontera = []
        heapq.heappush(frontera, ( 0, [copy.deepcopy(inicio)] ) )
        while len(frontera) > 0:
            actual = heapq.heappop(frontera)
            costo = actual[0]
            y = int(actual[1][len(actual[1]) - 1][0])
            x = int(actual[1][len(actual[1]) - 1][1])
            if y == int(final[0]) and x == int(final[1]):
                break
            else:
                #Derecha
                if bool([y, x + 1] in visitados) == False and x + 1 < self._tamanio:
                    a = copy.deepcopy(actual[1])
                    a.append([y, x + 1])
                    heapq.heappush(frontera, (costo + self._cromosoma[y, x + 1], a))
                # Abajo
                if bool([y + 1, x] in visitados) == False and y + 1 < self._tamanio:
                    a = copy.deepcopy(actual[1])
                    a.append([y + 1, x])
                    heapq.heappush(frontera, (costo + self._cromosoma[y + 1, x], a))
                # Izquierda
                if bool([y, x - 1] in visitados) == False and x - 1 >= 0:
                    a = copy.deepcopy(actual[1])
                    a.append([y, x - 1])
                    heapq.heappush(frontera, (costo + self._cromosoma[y, x - 1], a))
                # Arriba
                if bool([y - 1, x] in visitados) == False and y - 1 >= 0:
                    a = copy.deepcopy(actual[1])
                    a.append([y - 1, x])
                    heapq.heappush(frontera, (costo + self._cromosoma[y - 1, x], a))
                visitados.append([y, x])
        return actual